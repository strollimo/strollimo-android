package com.strollimo.android.models;

import android.os.Parcel;
import android.os.Parcelable;
import com.google.gson.annotations.Expose;

public class Location implements Parcelable {
    @Expose
    private double lat;
    @Expose
    private double lng;
    @Expose
    private double radius;

    public Location(double lat, double lng) {
        this(lat, lng, 0.0);
    }

    public Location(double lat, double lng, double radius) {
        this.lat = lat;
        this.lng = lng;
        this.radius = radius;
    }

    public double getLng() {
        return lng;
    }

    public void setLng(double lng) {
        this.lng = lng;
    }

    public double getLat() {
        return lat;
    }

    public void setLat(double lat) {
        this.lat = lat;
    }

    public double getRadius() {
        return radius;
    }

    public void setRadius(double radius) {
        this.radius = radius;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeDouble(lat);
        dest.writeDouble(lng);
        dest.writeDouble(radius);
    }

    public static final Parcelable.Creator<Location> CREATOR
            = new Parcelable.Creator<Location>() {
        public Location createFromParcel(Parcel in) {
            return new Location(in);
        }

        public Location[] newArray(int size) {
            return new Location[size];
        }
    };

    private Location(Parcel in) {
        lat = in.readDouble();
        lng = in.readDouble();
        radius = in.readDouble();
    }

}
