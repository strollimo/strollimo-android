package com.strollimo.android.models;

import com.google.gson.annotations.Expose;

public class TextClue extends Clue {
    public final static String TYPE = "textClue";

    @Expose
    private String text;
}
