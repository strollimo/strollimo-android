package com.strollimo.android.models.network.responses;

import com.google.gson.annotations.Expose;
import com.strollimo.android.models.Quest;

import java.util.List;

public class GetQuestResponse extends BaseResponse {
    @Expose
    private List<Quest> body;

    public List<Quest> getBody() {
        return body;
    }

    public void setBody(List<Quest> body) {
        this.body = body;
    }
}
