package com.strollimo.android.models;

import com.google.gson.annotations.Expose;

public class ImageClue extends Clue {
    public final static String TYPE = "imgClue";

    @Expose
    private String imgUrl;

    public String getImageUrl() {
        return imgUrl;
    }
}
