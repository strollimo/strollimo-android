package com.strollimo.android.models.network.requests;

public class GetMysteriesRequest extends BaseGetAccomplishableRequest {
    public GetMysteriesRequest(RequestHeader header, String envTag) {
        super(header, "mission", envTag, null);
    }
}
