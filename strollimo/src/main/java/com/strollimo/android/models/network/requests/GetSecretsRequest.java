package com.strollimo.android.models.network.requests;

public class GetSecretsRequest extends BaseGetAccomplishableRequest {
    public GetSecretsRequest(RequestHeader header, String childrenOf) {
        super(header, "secret", null, childrenOf);

    }
}
