package com.strollimo.android.models;

import com.google.gson.annotations.Expose;

public abstract class Clue {
    @Expose
    private String type;
    @Expose
    private int penaltyPoint;
}
