package com.strollimo.android.models.network.requests;

public class GetQuestsRequest extends BaseGetAccomplishableRequest {
    public GetQuestsRequest(RequestHeader header, String envTag) {
        super(header, "quest", envTag, null);
    }
}
