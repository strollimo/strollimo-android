package com.strollimo.android.models.network.requests;

import com.google.gson.annotations.Expose;

import java.util.ArrayList;
import java.util.List;

public class BaseGetAccomplishableRequest extends BaseRequest {

    @Expose
    private RequestBody body;

    public BaseGetAccomplishableRequest(RequestHeader header, String type, String envTag, String childrenOf) {
        super(header, "getAccomplishables");
        this.body = new RequestBody(type, envTag, childrenOf);
    }

    private static class RequestBody {
        @Expose
        private List<String> envTags = new ArrayList<String>();
        @Expose
        private String type;
        @Expose
        private String childrenOf;

        public RequestBody(String type, String envTag, String childrenOf) {
            this.type = type;
            this.envTags.add(envTag);
            this.childrenOf = childrenOf;
        }
    }
}
