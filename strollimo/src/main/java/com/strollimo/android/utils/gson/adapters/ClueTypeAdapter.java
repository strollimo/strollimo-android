package com.strollimo.android.utils.gson.adapters;

import com.google.gson.*;
import com.strollimo.android.models.Clue;
import com.strollimo.android.models.ImageClue;
import com.strollimo.android.models.TextClue;

import java.lang.reflect.Type;

public class ClueTypeAdapter implements JsonSerializer<Clue>, JsonDeserializer<Clue> {

    @Override
    public JsonElement serialize(Clue src, Type typeOfSrc, JsonSerializationContext context) {
        return context.serialize(src);
    }

    @Override
    public Clue deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext context)
            throws JsonParseException {
        JsonObject jsonObject = json.getAsJsonObject();
        String type = jsonObject.get("type").getAsString();
        if (ImageClue.TYPE.equals(type)) {
            return context.deserialize(jsonObject, ImageClue.class);
        } else if (TextClue.TYPE.equals(type)) {
            return context.deserialize(jsonObject, TextClue.class);
        } else {
            return null;
        }
    }
}
