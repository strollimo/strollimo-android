package com.strollimo.android.utils;

import android.content.Context;
import android.content.res.TypedArray;
import android.util.TypedValue;

public class ViewUtils {
    public static int getActionBarSize(Context context) {
        int[] attribute = new int[] { android.R.attr.actionBarSize };
        TypedValue typedValue = new TypedValue();
        TypedArray array = context.obtainStyledAttributes(typedValue.resourceId, attribute);
        return array.getDimensionPixelSize(0, -1);
    }
}
