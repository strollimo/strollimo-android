package com.strollimo.android.utils;

import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;

public class LocationUtils {
    private final static double EARTH_RADIUS_IN_KM = 6371.01;

    public static LatLngBounds getBoundingBox(LatLng point, double radiusInMeters) {
        GeoLocation loc = GeoLocation.fromDegrees(point.latitude, point.longitude);
        double radiusInKm = radiusInMeters/1000;
        GeoLocation[] resultGeoLoc = loc.boundingCoordinates(radiusInKm, EARTH_RADIUS_IN_KM);
        LatLng southWest = new LatLng(resultGeoLoc[0].getLatitudeInDegrees(), resultGeoLoc[0].getLongitudeInDegrees());
        LatLng northEast = new LatLng(resultGeoLoc[1].getLatitudeInDegrees(), resultGeoLoc[1].getLongitudeInDegrees());
        return new LatLngBounds(southWest, northEast);
    }
}
