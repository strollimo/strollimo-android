package com.strollimo.android.utils.test;

import android.util.Log;
import com.strollimo.android.StrollimoApplication;
import com.strollimo.android.core.EndpointsController;
import com.strollimo.android.models.*;
import com.strollimo.android.models.network.responses.*;
import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

import java.util.Map;

/**
 * Sample strollimo api calls
 */
public class NetworkTest {
    private static final String TAG = NetworkTest.class.getSimpleName();

    public static void testGetQuests() {
        StrollimoApplication.getService(EndpointsController.class).getQuests("cv", new Callback<GetQuestResponse>() {

            @Override
            public void success(GetQuestResponse getQuestsResponse, Response response) {
                for (Quest quest : getQuestsResponse.getBody()) {
                    Log.i(TAG, quest.getName());
                }
            }

            @Override
            public void failure(RetrofitError retrofitError) {
                Log.i(TAG, "Error: " + retrofitError.toString());
            }
        });
    }

    public static void testGetPickupStates() {
        StrollimoApplication.getService(EndpointsController.class).getPickupStatus(null, new Callback<GetPickupStatusResponse>() {

            @Override
            public void success(GetPickupStatusResponse getPickupStatusResponse, Response response) {
                Map<String, BaseAccomplishable.PickupState> statuses = getPickupStatusResponse.getSecretStatuses();
                for (String stringId : statuses.keySet()) {
                    Log.i(TAG, "id: " + stringId + ", value: " + statuses.get(stringId));
                }
            }

            @Override
            public void failure(RetrofitError retrofitError) {
                Log.i(TAG, "Error: " + retrofitError.toString());
            }
        });
    }

    public static void testPickup() {
        StrollimoApplication.getService(EndpointsController.class).pickupSecret(new Secret("11marco", "test"), "http//", new Callback<PickupSecretResponse>() {
            @Override
            public void success(PickupSecretResponse pickupSecretResponse, Response response) {
                Log.i(TAG, "success");
            }

            @Override
            public void failure(RetrofitError retrofitError) {
                Log.i(TAG, "failure");

            }
        });
    }

    public static void testGetMysteries() {
        StrollimoApplication.getService(EndpointsController.class).getMysteries("default", new Callback<GetMysteriesResponse>() {
            @Override
            public void success(GetMysteriesResponse getMysteriesResponse, Response response) {
                Log.i(TAG, "success");
            }

            @Override
            public void failure(RetrofitError retrofitError) {
                Log.i(TAG, "failure");

            }
        });
    }

    public static void testGetSecrets() {
        String mysteryId = "feb1eb63-fca6-4291-99c1-7a6fee31ee05";
        StrollimoApplication.getService(EndpointsController.class).getSecrets(mysteryId, new Callback<GetSecretsResponse>() {
            @Override
            public void success(GetSecretsResponse getSecretsResponse, Response response) {
                Log.i(TAG, "success");
            }

            @Override
            public void failure(RetrofitError retrofitError) {
                Log.i(TAG, "failure");

            }
        });
    }

    public static void testUpdateSecretCall() {
        Secret secret = new Secret("1", "bb test secret");
        secret.setShortDesc("short desc");
        secret.setImgUrl("test imag url");
        secret.setLocation(new Location(0.4, 0.5, 0.1));
        ImageComparisonPickupMode mode = new ImageComparisonPickupMode();
        mode.addUrl("test url 1");
        secret.addPickupMode(mode);
        StrollimoApplication.getService(EndpointsController.class).updateSecret(secret, new Callback<UpdateSecretResponse>() {
            @Override
            public void success(UpdateSecretResponse updateSecretResponse, Response response) {
                Log.i(TAG, "success");
            }

            @Override
            public void failure(RetrofitError retrofitError) {
                Log.i(TAG, "failure");

            }
        });
    }

    public static void testUpdateMysteryCall() {
        Mystery mystery = new Mystery("56", "BBTestNew", 0.4, 0.5, "image URL");
        mystery.setShortDesc("something");
        StrollimoApplication.getService(EndpointsController.class).updateMystery(mystery, new Callback<UpdateMysteryResponse>() {
            @Override
            public void success(UpdateMysteryResponse updateMysteryResponse, Response response) {
                Log.i(TAG, "success");
            }

            @Override
            public void failure(RetrofitError retrofitError) {
                Log.i(TAG, "failure");

            }
        });
    }
}
