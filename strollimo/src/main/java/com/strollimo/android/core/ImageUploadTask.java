package com.strollimo.android.core;

import android.graphics.Bitmap;
import android.os.Handler;
import android.os.Looper;
import android.util.Log;
import com.squareup.tape.Task;
import com.strollimo.android.StrollimoApplication;
import com.strollimo.android.models.network.AmazonUrl;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.InputStream;

public class ImageUploadTask implements Task<ImageUploadTask.Callback> {
    private static final long serialVersionUID = 126142781146165256L;

    private static final String TAG = "Tape:ImageUploadTask";
    private static final Handler MAIN_THREAD = new Handler(Looper.getMainLooper());

    public interface Callback {
        void onSuccess(String url);
        void onFailure();
    }

    private final String mSecretId;
    private final AmazonUrl mAmazonUrl;
    private final Bitmap mBitmap;

    public ImageUploadTask(String secretId, AmazonUrl amazonUrl, Bitmap bitmap) {
        mSecretId = secretId;
        mAmazonUrl = amazonUrl;
        mBitmap = bitmap;
    }

    @Override
    public void execute(final Callback callback) {
        // Image uploading is slow. Execute HTTP POST on a background thread.
        new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    ByteArrayOutputStream os = new ByteArrayOutputStream();
                    mBitmap.compress(Bitmap.CompressFormat.JPEG, 100, os);
                    InputStream inputStream = new ByteArrayInputStream(os.toByteArray(), 0, os.size());

                    AmazonS3Controller amazonS3Controller = StrollimoApplication.getService(AmazonS3Controller.class);
                    amazonS3Controller.uploadStream(mAmazonUrl.getBucket(), mAmazonUrl.getPath(), inputStream, os.size());

                    Log.i(TAG, "Upload success! " + mAmazonUrl.getUrl());

                    StrollimoApplication.getService(EndpointsController.class).getPickupSecret(mSecretId, mAmazonUrl.getUrl());


                    // Get back to the main thread before invoking a callback.
                    MAIN_THREAD.post(new Runnable() {
                        @Override
                        public void run() {
                            callback.onSuccess(mAmazonUrl.getUrl());
                        }
                    });

                } catch (RuntimeException e) {
                    // Get back to the main thread before invoking a callback.
                    MAIN_THREAD.post(new Runnable() {
                        @Override
                        public void run() {
                            Log.i(TAG, "Upload failed :( Will retry.");
                            callback.onFailure();
                        }
                    });
                    // remember retrofit error
                }
            }
        }).start();
    }
}