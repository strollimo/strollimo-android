package com.strollimo.android.core;

import com.strollimo.android.models.network.requests.*;
import com.strollimo.android.models.network.responses.*;
import retrofit.Callback;
import retrofit.http.Body;
import retrofit.http.POST;

public interface EndpointsInterface {
    @POST("/rest/operation/getAccomplishables")
    void getMysteries(@Body GetMysteriesRequest body, Callback<GetMysteriesResponse> callback);

    @POST("/rest/operation/getAccomplishables")
    GetMysteriesResponse getMysteries(@Body GetMysteriesRequest body);

    @POST("/rest/operation/getAccomplishables")
    GetSecretsResponse getSecrets(@Body GetSecretsRequest body);

    @POST("/rest/operation/getAccomplishables")
    void getSecrets(@Body GetSecretsRequest body, Callback<GetSecretsResponse> callback);

    @POST("/rest/operation/pickup")
    PickupSecretResponse getPickupSecret(@Body PickupSecretRequest body);

    @POST("/rest/operation/updateAccomplishable")
    void updateMystery(@Body UpdaterMysteryRequest body, Callback<UpdateMysteryResponse> callback);

    @POST("/rest/operation/updateAccomplishable")
    void updateSecret(@Body UpdaterSecretRequest body, Callback<UpdateSecretResponse> callback);

    @POST("/rest/operation/pickup")
    void pickupSecret(@Body PickupSecretRequest body, Callback<PickupSecretResponse> callback);

    @POST("/rest/operation/getPickupStates")
    void getPickupStatus(@Body GetPickupStatusRequest body, Callback<GetPickupStatusResponse> callback);

    @POST("/rest/operation/getAccomplishables")
    void getQuests(@Body GetQuestsRequest body, Callback<GetQuestResponse> callback);
}
