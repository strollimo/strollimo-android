package com.strollimo.android.ui.activities;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.NavUtils;
import android.util.Log;
import android.view.MenuItem;
import android.widget.ImageView;
import com.android.volley.toolbox.ImageLoader;
import com.strollimo.android.R;
import com.strollimo.android.StrollimoApplication;
import com.strollimo.android.core.AccomplishableController;
import com.strollimo.android.core.AmazonS3Controller;
import com.strollimo.android.core.VolleyImageLoader;
import com.strollimo.android.models.Clue;
import com.strollimo.android.models.ImageClue;
import com.strollimo.android.models.Secret;

public class ShowClueActivity extends Activity {
    public static final String EXTRA_SECRET_ID = "secret_id";
    public static final String TAG = ShowClueActivity.class.getSimpleName();

    // dependencies
    private AccomplishableController mAccomplishableController;

    // state
    private Secret mSecret;

    // views
    private ImageView mClueImageView;

    public static Intent createIntent(Context context, String secretId) {
        Intent intent = new Intent(context, ShowClueActivity.class);
        intent.putExtra(EXTRA_SECRET_ID, secretId);
        return intent;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        mAccomplishableController = StrollimoApplication.getService(AccomplishableController.class);

        setContentView(R.layout.show_clue_activity);
        getActionBar().setDisplayHomeAsUpEnabled(true);

        mClueImageView = (ImageView)findViewById(R.id.clue_image);

        mSecret = getSecret(getIntent(), savedInstanceState);
        if (mSecret == null) {
            finish();
            return;
        }
        showFirstImageClue(mSecret);
    }

    private void showFirstImageClue(Secret mSecret) {
        ImageClue clue = getFirstImageClue(mSecret);
        if (clue != null) {
            Log.d(TAG, String.format("Loading clue: %s", clue.getImageUrl()));
            String imageUrl = StrollimoApplication.getService(AmazonS3Controller.class).getUrl(clue.getImageUrl());
            VolleyImageLoader.getInstance().get(imageUrl, ImageLoader.getImageListener(mClueImageView, R.drawable.transparent_bg, R.drawable.transparent_bg));
        }
    }

    private ImageClue getFirstImageClue(Secret mSecret) {
        if (mSecret != null && mSecret.getClues() != null) {
            for (Clue clue : mSecret.getClues())  {
                if (clue instanceof ImageClue) {
                    return (ImageClue)clue;
                }
            }
        }

        return null;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                NavUtils.navigateUpFromSameTask(this);
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        outState.putString(EXTRA_SECRET_ID, mSecret.getId());
        super.onSaveInstanceState(outState);
    }

    private Secret getSecret(Intent intent, Bundle savedInstanceState) {
        Bundle bundle = null;
        if (savedInstanceState != null && savedInstanceState.containsKey(EXTRA_SECRET_ID)) {
            bundle = savedInstanceState;
        } else if (intent != null) {
            bundle = intent.getExtras();
        }
        if (bundle != null) {
            return getSecret(bundle);
        } else {
            return null;

        }
    }

    private Secret getSecret(Bundle bundle) {
        String secretId = bundle.getString(EXTRA_SECRET_ID);
        return mAccomplishableController.getSecretById(secretId);
    }
}
