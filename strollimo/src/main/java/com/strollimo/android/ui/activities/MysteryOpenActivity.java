package com.strollimo.android.ui.activities;

import android.app.ActionBar;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import com.strollimo.android.R;
import com.strollimo.android.StrollimoApplication;
import com.strollimo.android.core.AccomplishableController;
import com.strollimo.android.core.PreferencesController;
import com.strollimo.android.models.Mystery;
import com.strollimo.android.ui.fragments.MysterySecretsFragment;
import com.strollimo.android.ui.fragments.MysterySplashFragment;

public class MysteryOpenActivity extends AbstractTrackedFragmentActivity {
    public static final String EXTRA_SELECTED_MYSTERY_ID = "selected_mystery_id";
    private static final String TAG = MysteryOpenActivity.class.getSimpleName();
    private AccomplishableController mAccomplishableController;
    private PreferencesController mPrefs;
    private Mystery mCurrentMystery;
    private ViewPager mMainViewPager;
    private ViewPager mSecretsViewPager;
    private PagerAdapter mSecretsPagerAdapter;
    private MysterySecretsFragment mMysterySecretsFragment;

    public static Intent createDetailsIntent(Context context, String mysteryId) {
        Intent intent = new Intent(context, MysteryOpenActivity.class);
        intent.putExtra(EXTRA_SELECTED_MYSTERY_ID, mysteryId);
        return intent;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        mAccomplishableController = StrollimoApplication.getService(AccomplishableController.class);
        mPrefs = StrollimoApplication.getService(PreferencesController.class);

        getActionBar().setDisplayShowTitleEnabled(false);
        initalizeCurrentMystery(savedInstanceState, getIntent());

        ActionBar actionBar = getActionBar();
        actionBar.setDisplayHomeAsUpEnabled(true);

        mMainViewPager = new ViewPager(this) {

            private float lastX;

            @Override
            public boolean onInterceptTouchEvent(MotionEvent ev) {
                final int action = ev.getAction();
                switch (action) {
                    case MotionEvent.ACTION_DOWN:
                        lastX = ev.getX();
                        break;

                    case MotionEvent.ACTION_MOVE:
                        if (getCurrentItem() != 0) {
                            if (mMysterySecretsFragment.getViewPager() != null && mMysterySecretsFragment.getViewPager().getCurrentItem() == 0) {
                                if (lastX <= ev.getX()) {
                                    return super.onInterceptTouchEvent(ev);
                                } else {
                                    return false;
                                }
                            } else {
                                return false;
                            }
                        }
                }

                return super.onInterceptTouchEvent(ev);
            }
        };
        mMainViewPager.setPageTransformer(true, new ViewPager.PageTransformer() {

            @Override
            public void transformPage(View view, float position) {
                int pageWidth = view.getWidth();

                if (position < -1) { // [-Infinity,-1)
                    // This page is way off-screen to the left.
                    view.setAlpha(0);

                } else if (position <= 0) { // [-1,0]
                    // Use the default slide transition when moving to the left page
                    view.setTranslationX(0);

                } else if (position <= 1) { // (0,1]
                    // Counteract the default slide transition
                    view.setTranslationX(pageWidth * -position);

                } else { // (1,+Infinity]
                    // This page is way off-screen to the right.
                    view.setAlpha(0);
                }
            }
        });
        mMainViewPager.setId(R.id.mystery_main_viewpager);

        mMysterySecretsFragment = new MysterySecretsFragment(mCurrentMystery);
        mSecretsPagerAdapter = mMysterySecretsFragment.getPagerAdapter();
        mSecretsViewPager = mMysterySecretsFragment.getViewPager();
        MysterySplashFragment mysterySplashFragment = new MysterySplashFragment(mCurrentMystery, mMainViewPager);
        Fragment[] mainFragments = new Fragment[]{mysterySplashFragment, mMysterySecretsFragment};
        MysteryPagerAdapter mainAdapter = new MysteryPagerAdapter(getSupportFragmentManager(), mainFragments);
        mMainViewPager.setAdapter(mainAdapter);

        setContentView(mMainViewPager);
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        outState.putString(EXTRA_SELECTED_MYSTERY_ID, mCurrentMystery.getId());
        super.onSaveInstanceState(outState);
    }

    private void initalizeCurrentMystery(Bundle savedInstanceState, Intent intent) {
        String selectedMysteryId = null;
        if (savedInstanceState != null) {
            selectedMysteryId = savedInstanceState.getString(EXTRA_SELECTED_MYSTERY_ID);
        } else if (intent != null) {
            selectedMysteryId = intent.getStringExtra(EXTRA_SELECTED_MYSTERY_ID);
        }
        if (selectedMysteryId != null) {
            mCurrentMystery = mAccomplishableController.getMysteryById(getIntent().getStringExtra(EXTRA_SELECTED_MYSTERY_ID));
        }
        if (mCurrentMystery == null) {
            throw new NullPointerException("Selected mystery shouldn't be null");
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        Log.i(TAG, "Showing mystery: " + mCurrentMystery.getId());
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        if (mPrefs.isDebugModeOn()) {
            getMenuInflater().inflate(R.menu.main_options, menu);
        }
        getMenuInflater().inflate(R.menu.mystery_menu, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onPrepareOptionsMenu(Menu menu) {
        if (mPrefs.isDebugModeOn()) {
            menu.findItem(R.id.use_barcode).setChecked(mPrefs.isUseBarcode());
        }
        return super.onPrepareOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
            case R.id.use_barcode:
                boolean checked = item.isChecked();
                mPrefs.setUseBarcode(!checked);
                item.setChecked(!checked);
                return true;
            case R.id.add_secret:
                launchAddSecret();
                return true;
            case R.id.show_area:
                startActivity(AreaMapActivity.createAreaMapIntent(this, mCurrentMystery.getLocation()));
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    private void launchAddSecret() {
        Intent intent = new Intent(this, AddSecretActivity.class);
        intent.putExtra(EXTRA_SELECTED_MYSTERY_ID, mCurrentMystery.getId());
        startActivity(intent);
    }

    private static class MysteryPagerAdapter extends FragmentStatePagerAdapter {

        Fragment[] mFragments;

        public MysteryPagerAdapter(FragmentManager fm, Fragment... fragments) {
            super(fm);
            mFragments = fragments;
        }

        @Override
        public Fragment getItem(int i) {
            return mFragments[i];
        }

        @Override
        public int getCount() {
            return mFragments.length;
        }
    }
}
