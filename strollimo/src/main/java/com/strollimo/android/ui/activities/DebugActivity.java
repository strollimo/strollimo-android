package com.strollimo.android.ui.activities;

import android.app.Activity;
import android.os.Bundle;
import android.preference.ListPreference;
import android.preference.Preference;
import android.preference.PreferenceFragment;
import android.widget.Toast;
import com.strollimo.android.R;
import com.strollimo.android.StrollimoApplication;
import com.strollimo.android.core.AccomplishableController;
import com.strollimo.android.core.EndpointsController;
import com.strollimo.android.core.EnvSyncController;
import com.strollimo.android.core.PreferencesController;
import com.strollimo.android.utils.test.NetworkTest;


public class DebugActivity extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        // Display the fragment as the main content.
        getFragmentManager().beginTransaction().replace(android.R.id.content,
                new DebugFragment()).commit();
    }

    public static class DebugFragment extends PreferenceFragment {
        public static final String PREF_CURRENT_ENV = "pref_current_env";
        private PreferencesController mPrefs;
        private AccomplishableController mAccomplishableController;

        @Override
        public void onCreate(Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);
            mPrefs = StrollimoApplication.getService(PreferencesController.class);
            mAccomplishableController = StrollimoApplication.getService(AccomplishableController.class);

            // Load the preferences from an XML resource
            addPreferencesFromResource(R.xml.debug_prefs);
            final Preference changeEnvPref = findPreference(PREF_CURRENT_ENV);
            changeEnvPref.setSummary(mPrefs.getEnvTag());
            changeEnvPref.setOnPreferenceChangeListener(new Preference.OnPreferenceChangeListener() {
                @Override
                public boolean onPreferenceChange(Preference preference, Object newValue) {
                    changeEnvPref.setSummary((String)newValue);
                    mPrefs.setEnvTag((String)newValue);
                    syncData();
                    return true;
                }
            });

            final Preference debugModePref = findPreference(PreferencesController.DEBUG_MODE_ON);
            debugModePref.setOnPreferenceChangeListener(new Preference.OnPreferenceChangeListener() {
                @Override
                public boolean onPreferenceChange(Preference preference, Object newValue) {
                    mPrefs.setDebugModeOn((Boolean) newValue);
                    return true;
                }
            });

            final ListPreference sysEnvPref = (ListPreference) findPreference("pref_system_env");
            sysEnvPref.setSummary(sysEnvPref.getValue());
            sysEnvPref.setOnPreferenceChangeListener(new Preference.OnPreferenceChangeListener() {
                @Override
                public boolean onPreferenceChange(Preference preference, Object newValue) {
                    sysEnvPref.setSummary(newValue.toString());
                    PreferencesController.SystemEnv env = PreferencesController.SystemEnv.valueOf((String)newValue);
                    StrollimoApplication.getService(EndpointsController.class).reset(env);
                    return true;
                }
            });

            findPreference("nopref_crash_app").setOnPreferenceClickListener(new Preference.OnPreferenceClickListener() {
                @Override
                public boolean onPreferenceClick(Preference preference) {
                    throw new NullPointerException("Test crash");
                }
            });

            findPreference("nopref_test_something").setOnPreferenceClickListener(new Preference.OnPreferenceClickListener() {
                @Override
                public boolean onPreferenceClick(Preference preference) {
                    testSomething();
                    return true;
                }
            });
        }

        private void syncData() {
            String env = StrollimoApplication.getService(PreferencesController.class).getEnvTag();
            EnvSyncController preloader = new EnvSyncController(getActivity(), env, new AccomplishableController.OperationCallback() {
                @Override
                public void onSuccess() {
                    StrollimoApplication.getService(PreferencesController.class).saveSyncTime();
                }

                @Override
                public void onError(String errorMsg) {
                    Toast.makeText(getActivity(), "Sync error", Toast.LENGTH_SHORT).show();
                }
            });
            preloader.start();
        }

        private void testSomething() {
//        NetworkTest.testGetMysteries();
//        NetworkTest.testUpdateSecretCall();
//        NetworkTest.testUpdateMysteryCall();
//        NetworkTest.testGetSecrets();
//        NetworkTest.testPickup();
//        NetworkTest.testGetPickupStates();
            NetworkTest.testGetQuests();
        }
    }

}

