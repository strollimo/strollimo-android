package com.strollimo.android.ui.activities;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.NavUtils;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.CircleOptions;
import com.google.android.gms.maps.model.LatLng;
import com.strollimo.android.R;
import com.strollimo.android.models.Location;
import com.strollimo.android.utils.ViewUtils;

public class AreaMapActivity extends AbstractTrackedFragmentActivity {

    public static final String EXTRA_LOCATION_KEY = "location";
    public static final int DEFAULT_RADIUS = 40;
    private Location mLocation;
    private GoogleMap mMap;

    public static Intent createAreaMapIntent(Context context, Location location) {
        Intent intent = new Intent(context, AreaMapActivity.class);
        intent.putExtra(EXTRA_LOCATION_KEY, location);
        return intent;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.area_map_screen);
        mLocation = getLocationFromIntent(getIntent());
        if (mLocation == null) {
            finish();
            return;
        }

        getActionBar().setDisplayHomeAsUpEnabled(true);
        mMap = ((SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map)).getMap();
        initializeMap();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.area_map_menu, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.center:
                resetMap(true);
                return true;
            case android.R.id.home:
                NavUtils.navigateUpFromSameTask(this);
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    private Location getLocationFromIntent(Intent intent) {
        if (intent != null && intent.hasExtra(EXTRA_LOCATION_KEY)) {
            return intent.getParcelableExtra(EXTRA_LOCATION_KEY);
        } else {
            return null;
        }
    }

    private void initializeMap() {
        resetMap(false);
        LatLng center = new LatLng(mLocation.getLat(), mLocation.getLng());
        mMap.addCircle(new CircleOptions()
                .center(center)
                .radius(mLocation.getRadius() == 0f ? DEFAULT_RADIUS : mLocation.getRadius())
                .fillColor(getResources().getColor(R.color.map_circle))
                .strokeWidth(0)
        );
        mMap.getUiSettings().setCompassEnabled(true);
        mMap.setPadding(0, ViewUtils.getActionBarSize(this), 0, 0);
    }

    private void resetMap(boolean animate) {
        CameraPosition position = new CameraPosition.Builder().target(new LatLng(mLocation.getLat(), mLocation.getLng()))
                .zoom(18f)
                .bearing(0)
                .tilt(0)
                .build();
        CameraUpdate cameraUpdate = CameraUpdateFactory.newCameraPosition(position);
        if (animate) {
            mMap.animateCamera(cameraUpdate);
        } else {
            mMap.moveCamera(cameraUpdate);
        }
    }
}
